import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:travel_list/controller/data_controller.dart';
import 'package:travel_list/helper/color_helper.dart';
import 'package:travel_list/routes/AppRoutes.dart';
import 'package:travel_list/widgets/destination_card_widget.dart';
import 'package:travel_list/widgets/menu_header_widget.dart';
import 'package:travel_list/widgets/place_card_widget.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DataController>(builder: (controller) {
      return SafeArea(
          child: Container(
              padding: EdgeInsets.only(
                  left: Get.width * 0.03, right: Get.width * 0.03),
              child: Column(
                children: [
                  Container(
                      padding: EdgeInsets.only(top: Get.height * 0.01),
                      child: const MenuHeaderWidget()),
                  Container(
                      margin: EdgeInsets.only(
                          top: Get.height * 0.03, bottom: Get.height * 0.02),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text('Place',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 20)),
                          Container(
                              padding: const EdgeInsets.only(
                                  left: 10, right: 10, top: 5, bottom: 5),
                              decoration: BoxDecoration(
                                color: ColorHelper.yellow.withOpacity(0.5),
                                borderRadius: BorderRadius.circular(20)
                              ),
                              child: Text('Show All',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 15,
                                      color: ColorHelper.primary)))
                        ],
                      )),
                  SizedBox(
                    height: Get.height * 0.28,
                    child: GridView.count(
                      padding: const EdgeInsets.all(0),
                      crossAxisCount: 2,
                      crossAxisSpacing: 15,
                      mainAxisSpacing: 15,
                      childAspectRatio: 2.7,
                      physics: const BouncingScrollPhysics(),
                      children:
                          List.generate(controller.places.length, (index) {
                        return GestureDetector(
                          onTap: () {
                            controller.setSelectedPlace(controller.places[index]);
                            Get.toNamed(AppRoutes.DETAILS);
                          },
                          child: PlaceCardWidget(place: controller.places[index]),
                        );
                      }),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(
                          top: Get.height * 0.03, bottom: Get.height * 0.02),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text('Best Destination',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 20)),
                          Container(
                              padding: const EdgeInsets.only(
                                  left: 10, right: 10, top: 5, bottom: 5),
                              decoration: BoxDecoration(
                                color: ColorHelper.yellow.withOpacity(0.5),
                                borderRadius: BorderRadius.circular(20)
                              ),
                              child: Text('Show All',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 13,
                                      color: ColorHelper.primary)))
                        ],
                      )),
                  Expanded(
                      child: GridView.count(
                    crossAxisCount: 2,
                    crossAxisSpacing: 15,
                    mainAxisSpacing: 15,
                    childAspectRatio: 1,
                    physics: const BouncingScrollPhysics(),
                    children: List.generate(controller.destination.length, (index) {
                      return DestinationCardWidget(destination: controller.destination[index]);
                    }),
                  ))
                ],
              )));
    });
  }
}
