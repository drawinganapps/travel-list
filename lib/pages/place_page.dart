import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:travel_list/controller/data_controller.dart';
import 'package:travel_list/data/dummy_data.dart';
import 'package:travel_list/helper/color_helper.dart';
import 'package:travel_list/widgets/avatars_review_widget.dart';

class PlacePage extends StatelessWidget {
  const PlacePage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DataController>(builder: (controller) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: Get.height * 0.4,
            child: Stack(
              children: [
                Image.asset('assets/images/${controller.selectedPlace.picture}',
                    fit: BoxFit.fill, height: Get.height * 0.4),
                SafeArea(
                    child: Container(
                      margin: const EdgeInsets.only(top: 10),
                      padding: EdgeInsets.only(left: Get.width * 0.05, right: Get.width * 0.05),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              color: ColorHelper.white,
                              borderRadius: BorderRadius.circular(10)
                            ),
                            padding: const EdgeInsets.all(5),
                            child: Icon(Icons.arrow_back_ios_rounded, color: ColorHelper.dark),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              color: ColorHelper.white,
                              borderRadius: BorderRadius.circular(10)
                            ),
                            padding: const EdgeInsets.all(5),
                            child: Icon(Icons.bookmark_outline_rounded, color: ColorHelper.dark),
                          ),
                        ],
                      ),
                    )),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                top: Get.height * 0.03, bottom: Get.height * 0.02),
            padding: EdgeInsets.only(
                left: Get.width * 0.05, right: Get.width * 0.05),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${controller.selectedPlace.name} - NTB',
                    style: const TextStyle(
                        fontWeight: FontWeight.w600, fontSize: 28)),
                Container(margin: const EdgeInsets.only(bottom: 10)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: Icon(Icons.calendar_today_outlined,
                          color: ColorHelper.grey, size: 20),
                    ),
                    Text('5 Days',
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 15,
                            color: ColorHelper.grey))
                  ],
                )
              ],
            ),
          ),
          Container(
            width: Get.width,
            padding: EdgeInsets.only(
                left: Get.width * 0.05, right: Get.width * 0.05),
            child: Stack(children: [
              const Positioned(
                  child: AvatarReviewWidget(imageUrl: 'avatar_1.png')),
              const Positioned(
                  left: 25,
                  child: AvatarReviewWidget(imageUrl: 'avatar_2.png')),
              const Positioned(
                  left: 50,
                  child: AvatarReviewWidget(imageUrl: 'avatar_3.png')),
              const Positioned(
                  left: 75,
                  child: AvatarReviewWidget(imageUrl: 'avatar_4.png')),
              const Positioned(
                  left: 100,
                  child: AvatarReviewWidget(imageUrl: 'avatar_5.png')),
              Positioned(
                  left: 150,
                  child: Container(
                    height: Get.width * 0.1,
                    width: Get.width * 0.1,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.lightBlueAccent.withOpacity(0.1),
                        border: Border.all(color: Colors.blue)),
                    child: Center(
                      child: Text('+10',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: ColorHelper.grey)),
                    ),
                  )),
            ]),
          ),
          Expanded(
              child: ListView(
                padding: EdgeInsets.zero,
                physics: const BouncingScrollPhysics(),
                children: [
                  Container(
                    margin: EdgeInsets.only(top: Get.height * 0.03),
                    padding: EdgeInsets.only(
                        left: Get.width * 0.05, right: Get.width * 0.05),
                    child: Text(DummyData.copyText,
                        textAlign: TextAlign.justify,
                        style: const TextStyle(fontSize: 13, height: 1.2, letterSpacing: 1.0)),
                  )
                ],
              ))
        ],
      );
    });
  }
}
