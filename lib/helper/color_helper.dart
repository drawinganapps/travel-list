import 'package:flutter/material.dart';

class ColorHelper {
  static Color white = const Color.fromRGBO(255,255,255, 1);
  static Color whiteDarker = const Color.fromRGBO(246, 246, 246, 1);
  static Color primary = const Color.fromRGBO(237, 137, 54, 1);
  static Color secondary = const Color.fromRGBO(203, 51, 62, 1.0);
  static Color dark = const Color.fromRGBO(15,25,35, 1);
  static Color lightDark = const Color.fromRGBO(19, 33, 46, 1);
  static Color grey = const Color.fromRGBO(123, 137, 152, 1);
  static Color yellow = const Color.fromRGBO(246, 185, 0, 1);
}
