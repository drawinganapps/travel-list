import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:travel_list/helper/color_helper.dart';

ThemeData defaultTheme = ThemeData(
    brightness: Brightness.dark,
    scaffoldBackgroundColor: ColorHelper.whiteDarker,
    highlightColor: Colors.transparent,
    splashColor: Colors.transparent,
    primarySwatch: Colors.amber,
    textTheme: GoogleFonts.varelaRoundTextTheme().copyWith(
    ),
);
