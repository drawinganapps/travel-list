import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:travel_list/helper/color_helper.dart';
import 'package:travel_list/models/place_model.dart';

class PlaceCardWidget extends StatelessWidget {
  final PlaceModel place;

  const PlaceCardWidget({super.key, required this.place});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: ColorHelper.white, borderRadius: BorderRadius.circular(10)),
      child: Row(
        children: [
          Container(
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
            child: Image.asset(
              'assets/images/${place.picture}',
              width: Get.width * 0.2,
              height: Get.width * 0.18,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: Get.width * 0.03),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  child: Text(place.name,
                      style: const TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 12)),
                ),
                Row(
                  children: [
                    Icon(Icons.place_outlined,
                        color: ColorHelper.grey, size: 10),
                    Container(
                      margin: const EdgeInsets.only(left: 2, right: 2),
                      child: Text(place.destinations.toString(),
                          style: TextStyle(
                              color: ColorHelper.grey,
                              fontWeight: FontWeight.w600,
                              fontSize: 9)),
                    ),
                    Text('Destination',
                        style: TextStyle(
                            color: ColorHelper.grey,
                            fontWeight: FontWeight.w600,
                            fontSize: 9)),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
