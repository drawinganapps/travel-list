import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class AvatarReviewWidget extends StatelessWidget {
  final String imageUrl;
  const AvatarReviewWidget({super.key, required this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.width * 0.1,
      width: Get.width * 0.1,
      decoration: const BoxDecoration(
        shape: BoxShape.circle
      ),
      child: Image.asset('assets/images/${imageUrl}'),
    );
  }

}