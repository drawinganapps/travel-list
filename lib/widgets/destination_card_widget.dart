import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:travel_list/helper/color_helper.dart';
import 'package:travel_list/models/destination_model.dart';

class DestinationCardWidget extends StatelessWidget {
  final DestinationModel destination;

  const DestinationCardWidget({super.key, required this.destination});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: ColorHelper.white,
        borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
            child: Image.asset(
              'assets/images/${destination.picture}',
              width: Get.width * 1,
              height: Get.width * 0.25,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Get.width * 0.035),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 5),
                  child: Text(destination.name,
                      style: const TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 15)),
                ),
                Row(
                  children: [
                    Icon(Icons.place_outlined,
                        color: ColorHelper.grey, size: 10),
                    Container(
                      margin: const EdgeInsets.only(left: 2, right: 2),
                      child: Text(destination.location,
                          style: TextStyle(
                              color: ColorHelper.grey,
                              fontWeight: FontWeight.w600,
                              fontSize: 13)),
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
