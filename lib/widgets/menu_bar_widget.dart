import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:travel_list/helper/color_helper.dart';

class MenuBarWidget extends StatelessWidget {
  const MenuBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: Get.width * 0.06, right: Get.width * 0.06),
      decoration: BoxDecoration(
          color: ColorHelper.white,
          border: Border(
              top: BorderSide(
                  color: ColorHelper.dark.withOpacity(0.6), width: 0.2))),
      height: 90,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.home_rounded,
                color: ColorHelper.primary,
                size: 30,
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.search,
                color: ColorHelper.grey,
                size: 30,
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.bookmark_outline,
                color: ColorHelper.grey,
                size: 30,
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.person_outline,
                color: ColorHelper.grey,
                size: 30,
              )
            ],
          ),
        ],
      ),
    );
  }
}
