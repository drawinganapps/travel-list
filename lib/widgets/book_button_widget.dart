import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:travel_list/helper/color_helper.dart';

class BookButtonWidget extends StatelessWidget {
  const BookButtonWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height * 0.12,
      padding: EdgeInsets.only(left: Get.width * 0.1, right: Get.width * 0.1, top: Get.height * 0.01),
      decoration: BoxDecoration(color: ColorHelper.whiteDarker),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: Get.width * 0.80,
            height: Get.height * 0.07,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: ColorHelper.primary),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Explore Now',
                    style: TextStyle(
                        color: ColorHelper.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w600)),
              ],
            ),
          )
        ],
      ),
    );
  }

}
