import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:travel_list/helper/color_helper.dart';

class MenuHeaderWidget extends StatelessWidget {
  const MenuHeaderWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Container(
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
              ),
              margin: EdgeInsets.only(right: Get.width * 0.05),
              clipBehavior: Clip.antiAlias,
              child: Image.asset('assets/images/man.png',
                  fit: BoxFit.cover,
                  height: Get.height * 0.08,
                  width: Get.height * 0.08),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Morning',
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        color: ColorHelper.grey,
                        fontSize: 18)),
                Text('John Doe',
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: ColorHelper.dark,
                        fontSize: 24)),
              ],
            )
          ],
        ),
        Icon(
          Icons.notifications_outlined,
          color: ColorHelper.grey,
          size: 35,
        )
      ],
    );
  }
}
