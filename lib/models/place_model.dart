class PlaceModel {
  String name;
  String picture;
  int destinations;

  PlaceModel(this.name, this.picture, this.destinations);
}
