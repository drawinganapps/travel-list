class DestinationModel {
  String name;
  String picture;
  String location;

  DestinationModel(this.name, this.picture, this.location);
}
