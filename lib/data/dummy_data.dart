import 'package:travel_list/models/destination_model.dart';
import 'package:travel_list/models/place_model.dart';

class DummyData {
  static List<PlaceModel> places = [
    PlaceModel('Lombok', 'lombok.jpg', 10),
    PlaceModel('Manado', 'manado.jpg', 8),
    PlaceModel('Samosir', 'samosir.jpg', 3),
    PlaceModel('Yogyakarta', 'yogyakarta.jpg', 9),
    PlaceModel('Bali', 'bali.jpg', 12),
    PlaceModel('Jawa Timur', 'jawa_timur.jpg', 6),
  ];

  static List<DestinationModel> destinations = [
    DestinationModel('Danau Toba', 'danau_toba.jpg', 'Sumatra, Indonesia'),
    DestinationModel(
        'Danau Kelimutu', 'danau_kelimutu.jpg', 'Flores, Indonesia'),
    DestinationModel('Ubud', 'ubud.jpg', 'Bali, Indonesia'),
    DestinationModel('Mandalika', 'mandalika.jpg', 'Lombok, Indonesia'),
  ];

  static String copyText =
      'Pulau Lombok, Nusa Tenggara Baratmerupakan salah satu pulau yang memiliki ragam wisata terkhusus dengan kekayaan alamnya yang mengagumkan, mulai dari wisata ekstrim seperti wisata Gunung Rinjani, hingga wisata halal yang terkenal religi seperti wisata masjid Islamic Center.'
      '\n'
      '\n'
      '\n'
      'Pulau yang bersebelahan dengan Pulau Bali ini, tak kalah menarik karena pulau ini akan memanjakan siapa saja yang dating. Tak kalah dengan Bali, pulau Lombok juga memiliki pantai-pantai yang indah seperti Pantai Pink, Pantai Senggigi, juga pantai lain yang tersbar di pinggiran area Pulau Lombok.'
      '\n'
      '\n'
      '\n'
      'Lombok juga akan meningkatkan pamor nya dengan akan adanya pegelaran pesta akbar Moto GP tahun ini di Mandalika. Sirkuit yang akan mendatangkan pembalap-pembalap kelas dunia tersebut kini sudah hampir menyelesaikan proses pembangunnya sebesar 60%.'
      '\n'
      '\n'
      '\n'
      'Masih banyak lagi fakta-fakta menarik terkait Pulau Lombok, dilansir dari berbagai sumber, berikut fakta bebrapa fakta menarik Pulau Lombok.';
}
