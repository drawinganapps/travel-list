import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:travel_list/pages/dashboard_page.dart';
import 'package:travel_list/widgets/menu_bar_widget.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: DashboardPage(),
      bottomNavigationBar: MenuBarWidget(),
    );
  }

}
