import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:travel_list/pages/place_page.dart';
import 'package:travel_list/widgets/book_button_widget.dart';

class DetailPlaceScreen extends StatelessWidget {
  const DetailPlaceScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PlacePage(),
      bottomNavigationBar: const BookButtonWidget(),
    );
  }
}
