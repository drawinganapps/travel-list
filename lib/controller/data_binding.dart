import 'package:get/get.dart';
import 'package:travel_list/controller/data_controller.dart';

class DataBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DataController());
  }
}
