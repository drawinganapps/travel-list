import 'package:get/get.dart';
import 'package:travel_list/data/dummy_data.dart';
import 'package:travel_list/models/destination_model.dart';
import 'package:travel_list/models/place_model.dart';

class DataController extends GetxController {
  List<PlaceModel> _places = [];
  List<DestinationModel> _destination = [];
  late PlaceModel _selectedPlace;

  List<PlaceModel> get places => _places;

  List<DestinationModel> get destination => _destination;

  PlaceModel get selectedPlace => _selectedPlace;

  @override
  void onInit() {
    _initData();
    super.onInit();
  }

  void _initData() {
    _places = DummyData.places;
    _destination = DummyData.destinations;
    _selectedPlace = DummyData.places[0];
  }

  void setSelectedPlace(PlaceModel place) {
    _selectedPlace = place;
    update();
  }
}
