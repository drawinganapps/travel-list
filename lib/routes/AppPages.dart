import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:travel_list/controller/data_binding.dart';
import 'package:travel_list/screens/dashboard_screen.dart';
import 'package:travel_list/screens/detail_place_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.HOME,
        page: () => const DashboardScreen(),
        binding: DataBinding()),
    GetPage(name: AppRoutes.DETAILS, page: () => const DetailPlaceScreen()),
  ];
}
